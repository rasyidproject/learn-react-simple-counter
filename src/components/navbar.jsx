import React from 'react'

// Stateless Functional Component
const NavBar = ({ totalCounters }) => (  
  <nav className="navbar navbar-light bg-light">
    <a className="navbar-brand" href="#">
      NavBar{" "} 
      <span className="badge badge-pill badge-secondary">
        {totalCounters}
      </span>
    </a>
  </nav>
)

// Stateful Component
// class NavBar extends Component {
//   state = {}
//   render() {
//     return (  
//       <nav className="navbar navbar-light bg-light">
//         <a className="navbar-brand" href="#">
//           NavBar{" "} 
//           <span className="badge badge-pill badge-secondary">
//             {this.props.totalCounters}
//           </span>
//         </a>
//       </nav>
//     )
//   }
// }

export default NavBar;