import React, {Component} from 'react';
import NavBar from './components/navbar'
import Counters from './components/counters'
import './App.css';

class App extends Component {

  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 3 },
      { id: 3, value: 2 },
      { id: 4, value: 1 },
    ]
  }

  handleIncrement = (counter) => {
    const counters = [...this.state.counters]
    const index = counters.indexOf(counter)
    counters[index] = {...counter}
    counters[index].value++
    console.log(counter)
    this.setState({ counters })
  }

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter( c => c.id !== counterId)
    this.setState({ counters })
  }

  handleReset = () =>  {
    const counters = this.state.counters.map( c => {
      c.value = 0
      return c
    })
    this.setState({counters})
  }

  render(){
    return (
      <React.Fragment>
        <NavBar 
          totalCounters={this.state.counters
            .filter(c => c.value > 0)
            .map(e => e.value)
            .reduce((a,b) => a+b,0)} 
        />
        <main className="container">
          <Counters 
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
            counters={this.state.counters}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
